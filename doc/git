#
# Copyright (C) 2010-2016 Svetlana A. Tkachenko
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.

This page is a git cheatsheet. git is recommended for contributing to guppy, but is not required.

(As always, manpages are the grand superior resource.)

====== Starting ======

You can create an own git project inside your home directory using:

  mkdir project.git
  cd project.git
  git init --bare

The above command creates a so called "bare" repository in the project.git directory (it is just the contents of the "''.git''" directory, without any files checked out around it).


Now you can "push" your local git repository to the remote machine using:


  $ cd project
  
  $ git init
  $ git add *
  
  $ git commit -m "Initial commit"
  
  $ git remote add origin ssh://USERNAME@eu.gshellz.org:222/~USERNAME/project.git
  $ git push origin master


Please note, that the ''USERNAME'' should be replaced with your username on the corresponding server. In the future, you can easily push and pull all changes in this projects using git pull (to fetch the changes) or git push (to publish the changes in your remote repository).

====== Undoing commits ======


To undo commits, you need to set the branch head to an older commit. All next commits will be gone.

1) Find the sha of the last commit you want to keep. Do a

  $ git log

Each commit will be visible like this:

  commit 1e3487c2fdfd1e8299744a1e7fae4ca3867bc9b9
  Author: thewookie <thewookie@gshellz.org>
  Date:   Tue Aug 2 10:26:38 2011 +0930
  
      Test commit.


The "''1e3487c2fdfd1e8299744a1e7fae4ca3867bc9b9''" is the hash.

2) Reset the branch to the needed commit.

  $ git reset --hard 1e3487c2fdfd1e8299744a1e7fae4ca3867bc9b9''

If you use --soft switch, files will be unchanged.
If you use --hard switch, files be changed to match the state after the indicated commit. 

3) If you already pushed before, you will need to "force" the push to indicate that the loss of data (next commits) is intentional.

  $ git push -f

====== Formatted patches ======

git can save the changes you made to a file so someone can apply them later. That's why you don't need commit access - all you need is to send patches, and have developers apply them. It is a feature of git as opposed to svn.

To get a patch for your changes, you need to follow some easy steps.

1) Commit latest changes.

Do a git add for the new files, and then ''git -m "added foo to bar"'', or simply ''git -am "added foo to bar"'' to have git add and remove files to the repo according to filesystem changes automatically.


2) Find a previous revision number.

Look at git log and find a previous revision number.

    commit d45g67j8k9la2cvmz85a6858d58a5cb4c978b676
    Author: thewookie <thewookie@gshellz.org>
    Date:   Mon Oct 3 21:35:41 2011 +0200
  
        add foo to bar
  
    commit e183890d8f78ce45y6u7d4f5g6hj7j8gf05a30ad
    Author: thewookie <thewookie@gshellz.org>
    Date:   Sat Oct 1 23:09:44 2011 +0200
  
        test
  



3) git format-patch

Do a ''git format-patch revision_#_here''. You will see a filename as output. This is the file which contains your new patch. Example,

  $ git format-patch e183890d8f78ce45y6u7d4f5g6hj7j8gf05a30ad
  0001-add-foo-to-bar.patch
  $ head 0001-add-foo-to-bar.patch
  From d45g67j8k9la2cvmz85a6858d58a5cb4c978b676 Mon Sep 17 00:00:00 2001
  From: thewookie <thewookie@gshellz.org>
  Date: Mon, 3 Oct 2011 21:35:41 +0300
  Subject: [PATCH] add foo to bar
  
  ---
   .gitignore        |    2 +
   bar               |   52 ++++++++++++++++++++++++++++++++++++++++++++++++--
   bar.conf.dist     |   54 +++++++++++++++++++++++++++++++++++++++++++++++++++++
   3 files changed, 105 insertions(+), 3 deletions(-)
  create mode 100644 .gitignore
  
  diff --git a/.gitignore b/.gitignore
  new file mode 100644
  $

4) git am

To apply a patch, use **''git am''**, replacing ''patchname.patch'' with a real file name.

  $ git am --signoff < patchname.patch
  Applying: add foo to bar


====== Links ======

  * [[http://sethrobertson.github.com/GitPostProduction/gpp.html|Post-production editing using git]]
