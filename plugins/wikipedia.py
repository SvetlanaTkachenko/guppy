# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2011 FurryHead <furryhead14@yahoo.com>
# Copyright (C) 2011 Kenneth K. Sham <eureka@divinitycoding.net>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.


import xml.etree.ElementTree
import urllib.request, urllib.parse


@plugin
class Wikipedia(object):
    def __init__(self, server):
        self.server = server
        self.commands = ["wp"]
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if cmd == "wp":
            if len(args) < 1:
                self.server.doMessage(channel, user + ": Not enough arguments.")
                return
            url = "http://en.wikipedia.org/w/api.php?action=opensearch&limit=3&namespace=0&format=xml&search="
            query = " ".join(args)
            search = url + urllib.parse.quote_plus(query, "/")
            try:
                # get the data
                data = urllib.request.urlopen(search).readlines()
                # parse the xml to a xpath tree
                xml_tree = xml.etree.ElementTree.fromstringlist(data)
                # get <Items> tags
                # the first element in xml_tree is a <QUERY> tag
                # we don't need that. We want the <SECTION> tag, which is the
                # second tag, hence the index of 1.
                xml_Items = list(xml_tree[1])
                # loop and grab all <Description> tags
                # and have an array to store the results
                results = []
                for item in xml_Items:
                    this = list(item)
                    results.append(this[1].text.strip('\n') + ' ' + this[2].text.strip('\n'))
                if not results:                                                          ######## check 
                    self.server.doMessage(channel, "No results for " + query + ".")      ########  for
                    return                                                               ######## results
                
                for result in results:
                    self.server.doMessage(channel, result)
            except Exception:
                pass
    
