# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2010-2016 FurryHead <furryhead14@yahoo.com>
# Copyright (C) 2011 Kenneth K. Sham <ken@ksham.net>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.

import os, configparser


@plugin
class Auth(object):
    """Manage user permissions and user data."""
    def __init__(self, server):
        self.server = server
        self.commands = ["addowner", "delowner", "addadmin", "addmod", "deladmin", "delmod", "admins", "mods", "owners", "passwd", "identify"]
        self.owners = {}
        self.admins = {}
        self.mods = {}
        self.server.handle("command", self.handle_command, self.commands)
        self.server.handle("part", self.handle_part)
        self.server.handle("quit", self.handle_quit)
        self.server.handle("nick", self.handle_nick)
        self.authfile = self.server.config["confdir"] + "auth.cfg"
        self._loadusers()

    def _checkAuth(self, user):
        stillOn = False
        for ch in list(self.server.userlist.keys()):
            if user in self.server.userlist[ch]:
                stillOn = True

        if not stillOn:
            if user.lower() in list(self.owners.keys()):
                self.owners[user.lower()][1] = False
            elif user.lower() in list(self.admins.keys()):
                self.admins[user.lower()][1] = False
            elif user.lower() in list(self.mods.keys()):
                self.mods[user.lower()][1] = False

    def handle_quit(self, user, message):
        self._checkAuth(user)

    def handle_part(self, channel, user, message):
        self._checkAuth(user)

    def handle_nick(self, oldnick, newnick):
        if self.isOwner(oldnick):
            self.owners[newnick.lower()] = self.owners[oldnick.lower()]
            del self.owners[oldnick.lower()]
        elif self.isAdmin(oldnick):
            self.admins[newnick.lower()] = self.admins[oldnick.lower()]
            del self.admins[oldnick.lower()]
        elif self.isMod(oldnick):
            self.mods[newnick.lower()] = self.mods[oldnick.lower()]
            del self.mods[oldnick.lower()]

    def handle_command(self, channel, user, cmd, args):
        if cmd == "owners":
            self.server.doMessage(channel, user + ": My owners are: " + " ".join(self.owners))
        elif cmd == "admins":
            self.server.doMessage(channel, user + ": My administrators are: " + " ".join(self.admins))
        elif cmd == "mods":
            self.server.doMessage(channel, user + ": My moderators are: " + " ".join(self.mods))
        elif cmd == "passwd":
            if channel == user:
                if user.lower() in list(self.owners.keys()):
                    if self.owners[user.lower()][1]:
                        if len(args) < 1:
                            self.server.doMessage(channel, user + ": Not enough arguments.")
                            return

                        if len(args) >= 2:
                            self.owners[user.lower()][0] = args[1]
                            self.server.doMessage(channel, user + ": Password successfully changed.")
                        else:
                            self.owners[user.lower()][0] = args[0]
                            self.server.doMessage(channel, user + ": Password successfully changed.")

                elif user.lower() in list(self.admins.keys()):
                    if self.admins[user.lower()][1]:
                        if len(args) < 1:
                            self.server.doMessage(channel, user + ": Not enough arguments.")
                            return

                        if len(args) >= 2:
                            self.admins[user.lower()][0] = args[1]
                            self.server.doMessage(channel, user + ": Password successfully changed.")
                        else:
                            self.admins[user.lower()][0] = args[0]
                            self.server.doMessage(channel, user + ": Password successfully changed.")

                elif user.lower() in list(self.mods.keys()):
                    if self.mods[user.lower()][1]:
                        if len(args) < 1:
                            self.server.doMessage(channel, user + ": Not enough arguments.")
                            return

                        if len(args) >= 2:
                            self.mods[user.lower()][0] = args[1]
                            self.server.doMessage(channel, user + ": Password successfully changed.")
                        else:
                            self.mods[user.lower()][0] = args[0]
                            self.server.doMessage(channel, user + ": Password successfully changed.")

                else:
                    self.server.doMessage(channel, user + ": You are not in the owners/admins/mods lists.")
                    return
            else:
                self.server.doMessage(channel, user + ": Please do that using private messaging.")
                return
        elif cmd == "identify":
            if len(args) < 1:
                self.server.doMessage(channel, user + ": Not enough arguments.")
                return
            if channel == user:
                if user.lower() in list(self.owners.keys()):
                    if self.owners[user.lower()][1]:
                        self.server.doMessage(channel, user + ": You are already identified.")
                    elif args[0] == self.owners[user.lower()][0]:
                        self.owners[user.lower()][1] = True
                        self.server.doMessage(channel, user + ": You are now identified for " + user)
                    else:
                        self.server.doMessage(channel, user + ": Invalid password.")
                elif user.lower() in list(self.admins.keys()):
                    if self.admins[user.lower()][1]:
                        self.server.doMessage(channel, user + ": You are already identified.")
                    elif args[0] == self.admins[user.lower()][0]:
                        self.admins[user.lower()][1] = True
                        self.server.doMessage(channel, user + ": You are now identified for " + user)
                    else:
                        self.server.doMessage(channel, user + ": Invalid password.")
                elif user.lower() in list(self.mods.keys()):
                    if self.mods[user.lower()][1]:
                        self.server.doMessage(channel, user + ": You are already identified.")
                    elif args[0] == self.mods[user.lower()][0]:
                        self.mods[user.lower()][1] = True
                        self.server.doMessage(channel, user + ": You are now identified for " + user)
                    else:
                        self.server.doMessage(channel, user + ": Invalid password.")
            else:
                self.server.doMessage(channel, user + ": Please do that using private messaging.")
                return

        elif self.isOwner(user):
            if len(args) < 1:
                self.server.doMessage(channel, user + ": What nick?")
                return

            if cmd == "addowner":
                if args[0].lower() in list(self.owners.keys()):
                    return
                self.owners[args[0].lower()] = ['', True]
            elif cmd == "addadmin":
                if args[0].lower() in list(self.admins.keys()):
                    return
                self.admins[args[0].lower()] = ['', True]
            elif cmd == "addmod":
                if args[0].lower() in list(self.mods.keys()):
                    return
                self.mods[args[0].lower()] = ['', True]
            elif cmd == "delowner":
                if args[0].lower() in list(self.owners.keys()):
                    del self.owners[args[0].lower()]
            elif cmd == "deladmin":
                if args[0].lower() in list(self.admins.keys()):
                    del self.admins[args[0].lower()]
            elif cmd == "delmod":
                if args[0].lower() in list(self.mods.keys()):
                    del self.mods[args[0].lower()]

    #save users
    def destroy(self):
        self.configParser = configparser.RawConfigParser()

        if os.path.isfile(self.authfile):
            self.configParser.read(self.authfile)

        fh = open(self.authfile, "w")
        network = self.server.config["network"]
        if not self.configParser.has_section(network):
            self.configParser.add_section(network)

        self.configParser.set(self.server.config["network"], "owners", ",".join([k + ":" + v[0] for k, v in list(self.owners.items())]))
        self.configParser.set(self.server.config["network"], "admins", ",".join([k + ":" + v[0] for k, v in list(self.admins.items())]))
        self.configParser.set(self.server.config["network"], "mods", ",".join([k + ":" + v[0] for k, v in list(self.mods.items())]))

        self.configParser.write(fh)
        fh.close()

    def _loadusers(self):
        network = self.server.config["network"]

        if os.path.isfile(self.authfile):
            self.configParser = configparser.RawConfigParser()
            self.configParser.read(self.authfile)

            if self.configParser.has_section(network):
                if self.configParser.has_option(network, "owners"):
                    olist = self.configParser.get(network, "owners").lower().split(",")
                    olist = [o for o in olist if o != '']
                    for user in olist:
                        nick, pw = user.split(":")
                        self.owners[nick] = [pw, pw == '']

                if self.configParser.has_option(network, "admins"):
                    alist = self.configParser.get(network, "admins").lower().split(",")
                    alist = [a for a in alist if a != '']
                    for user in alist:
                        nick, pw = user.split(":")
                        self.admins[nick] = [pw, pw == '']

                if self.configParser.has_option(network, "mods"):
                    mlist = self.configParser.get(network, "mods").lower().split(",")
                    mlist = [m for m in mlist if m != '']
                    for user in mlist:
                        nick, pw = user.split(":")
                        self.mods[nick] = [pw, pw == '']

        else:
            onick = self.server.config["owner_nick"].lower()
            self.owners[onick] = ['', True]

    def isOwner(self, user):
        return user.lower() in self.owners and self.owners[user.lower()][1]

    def isAdmin(self, user):
        if user.lower() in self.admins and self.admins[user.lower()][1]:
            return True
        else:
            return self.isOwner(user)

    def isMod(self, user):
        if user.lower() in self.mods and self.mods[user.lower()][1]:
            return True
        elif self.isAdmin(user):
            return True
        else:
            return self.isOwner(user)
