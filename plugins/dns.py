# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.

import urllib.request, urllib.parse, urllib.error
import socket


@plugin
class dns(object):
    """DNS lookups."""
    def __init__(self, server):
        self.server = server
        self.commands = ["dns"]
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if cmd == "dns":
            if len(args) < 1:
                self.server.doMessage(channel, user + ": Return a fully qualified domain name for a list of space-separated IPs or hostnames.")
                return
            try:
                for each in args:
                    self.server.doMessage(channel, user + ": " + each + " = " + socket.getfqdn(each))
            except Exception as e:
                self.server.doMessage(channel, user + ": " + str(e))
