# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2011 FurryHead <furryhead14@yahoo.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.


import urllib.request, urllib.parse, urllib.error


@plugin
class TinyURL(object):
    def __init__(self, server):
        self.server = server
        self.commands = ["tinyurl"]
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if cmd == "tinyurl":
            if len(args) < 1:
                self.server.doMessage(channel, user + ": Not enough arguments.")
                return

            url = args[0] if args[0].startswith("http://") else "http://" + args[0]
            self.server.doMessage(channel, user + ": " + urllib.request.urlopen("http://tinyurl.com/api-create.php?url=" + url).readline().decode('utf8'))
