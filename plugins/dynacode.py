# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2010-2016 Furryhead <furryhead14@yahoo.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.

import sys


@plugin
class DynaCode(object):
    def __init__(self, server):
        self.server = server
        self.server.pluginManager.loadPlugin("auth")
        self.commands = ["py"]
        self.ownerOnly = 1
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if self.server.getPlugin("auth").isOwner(user) and cmd == "py":
            if len(args) < 1:
                self.server.doMessage(channel, user + ": Not enough arguments.")
                return

            backup = sys.stdout
            myout = OutputBuffer()
            sys.stdout = myout
            try:
                exec(" ".join(args))
            except Exception as e:
                sys.stdout = backup
                self.server.doMessage(channel, user + ": " + e.__class__.__name__ + ": " + e.__str__())

            sys.stdout = backup
            for line in myout.getOutput():
                self.server.doMessage(channel, user + ": " + line)


class OutputBuffer(object):
    def __init__(self):
        self.__output = []

    def write(self, s):
        if s != "\n":
            self.__output.append(s)

    def getOutput(self):
        return self.__output
