# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2011 Nathan Hakak (aka DrKabob) <natman97@when.com>
# Copyright (C) 2011 aLaserShark <shark@alasershark.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.


# Provides a help command that lists all commands and functions ~Nat (DrKabob)


@plugin
class Help(object):
    """`help` to list commands, `help <command/plugin>` for plugin or command help."""
    def __init__(self, server):
        self.server = server
        self.commands = ["help"]
        self.server.handle("command", self.handle_command, self.commands)
#        self.server.handle("join", self.handle_join)

    def handle_command(self, channel, user, cmd, args):
            if cmd == "help":
                print(args)  # ['FIRST', 'SECOND']
                if not args:
                    self.server.doMessage(user, "All commands can be sent in a private message to the bot, or a channel message beginning with "
                        + self.server.config["comchar"] + " or " + self.server.config["nickname"] + ":")
                    # get command lists strings for each plugin
                    strings = []
                    for pluginName in self.server.plugins:
                        if not self.server.plugins[pluginName].commands:
                            strings.append(pluginName + ": (none)")
                        else:
                            strings.append(pluginName + ": " + ", ".join(self.server.plugins[pluginName].commands))
                    reply = ""
                    while True:
                        if not strings:
                            break
                        nextitem = strings.pop()
                        if reply == "":
                            reply += nextitem
                        elif len(reply + nextitem) > 60:
                            self.server.doMessage(user, reply)
                            reply = ""
                        else:
                            reply += "; " + nextitem
                    if reply != "":
                        self.server.doMessage(user, reply)
                    reply = ""
                    self.server.doMessage(user, "Ask 'help plugin' for plugin help.")
                else:
                    try:
                        reply = args[0] + " plugin. " + self.server.plugins[args[0]].__doc__
                    except KeyError:
                        reply = "Error: plugin " + args[0] + " not found."
                    except TypeError:
                        reply = "Plugin " + args[0] + " has no help."
                    if self.server.plugins[args[0]].commands:
                        reply += " Commands: " + ", ".join(self.server.plugins[args[0]].commands) + "."
                    else:
                        reply += " Commands: (none)."
                    self.server.doMessage(user, reply)
#    def handle_join(self, channel, user):
#        self.server.doNotice(user, "Hello! My name is " + self.server.config["nickname"] + ". If you want a list of commands, type " +
#            self.server.config["comchar"] + "help")
