# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2011 FurryHead <furryhead14@yahoo.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.


import random


@plugin
class Smack(object):
    def __init__(self, server):
        self.server = server
        self.commands = ["smack"]
        self.server.handle("command", self.handle_command, self.commands)
        self.objects = ["smelly fish", "tin pot", "frying pan", "mouse", "keyboard", "fly swatter"]

    def handle_command(self, channel, user, cmd, args):
        if cmd == "smack" and len(args) > 0:
            self.server.doAction(channel, "smacks %s with a %s!" % (args[0], self.objects[random.randint(0, len(self.objects) - 1)]))
