# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.


import sys


@plugin
class PluginLoader(object):
    def __init__(self, server):
        self.server = server
        self.server.pluginManager.loadPlugin("Auth")
        self.commands = ["load", "unload", "reload", "reloadall", "loaded", "allplugins"]
        self.ownerOnly = 1
        self.server.handle("command", self.handle_command, self.commands)

    def handle_command(self, channel, user, cmd, args):
        if self.server.getPlugin("auth").isMod(user):
            if cmd == "reloadall":
                self.server.pluginManager.unloadAllPlugins()
                self.server.pluginManager.loadAllPlugins()
                self.server.doMessage(channel, user + ": Reloaded all plugins")
                return
            elif cmd == "allplugins":
                self.server.doMessage(channel, user + ": Available plugins: " + " ".join(list(sys.modules["irc"].plugins.pList.keys())))
                return

            elif cmd == "loaded":                                               ########
                loadedPl = []                                                   ########
                for item in (sys.modules["irc"].plugins.pList.keys()):          ########
                    if self.server.pluginManager.loadedPlugin(item):            ########
                        loadedPl.append(str(item))                              ########
                sortedPl = sorted(loadedPl)                             ########  ADDED
                joinedPl = " | ".join(sortedPl)                                 ######## old: joinedPl = " | ".join(loadedPl)
                self.server.doMessage(channel, "Loaded plugins: " + joinedPl)   ########
                return

            if len(args) < 1:
                self.server.doMessage(channel, user + ": Not enough arguments.")
                return

            if cmd == "load":
                success, failed, dne = [], [], []
                for arg in args:
                    if self.server.pluginManager.pluginExists(arg):
                        if self.server.pluginManager.loadedPlugin(arg):
                            failed.append(arg)
                        else:
                            self.server.pluginManager.loadPlugin(arg)
                            success.append(arg)
                    else:
                        dne.append(arg)
                if len(dne):
                    self.server.doMessage(channel, user + ": Non-existent plugins: " + ", ".join(dne))
                if len(failed):
                    self.server.doMessage(channel, user + ": Already loaded plugins: " + ", ".join(failed))
                if len(success):
                    self.server.doMessage(channel, user + ": Successfully loaded plugins: " + ", ".join(success))

            elif cmd == "unload":
                for arg in args:
                    if self.server.pluginManager.pluginExists(arg):
                        if self.server.pluginManager.loadedPlugin(arg):
                            self.server.pluginManager.unloadPlugin(arg)
                            self.server.doMessage(channel, user + ": Successfully unloaded plugin " + arg)
                        else:
                            self.server.doMessage(channel, user + ": Plugin " + arg + " has not been loaded")
                    else:
                        self.server.doMessage(channel, user + ": No such plugin " + arg)
            elif cmd == "reload":
                for arg in args:
                    if self.server.pluginManager.pluginExists(arg):
                        if self.server.pluginManager.loadedPlugin(arg):
                            self.server.pluginManager.reloadPlugin(arg)
                            self.server.doMessage(channel, user + ": Successfully reloaded plugin " + arg)
                        else:
                            self.server.doMessage(channel, user + ": Plugin " + arg + " has not been loaded")
                    else:
                        self.server.doMessage(channel, user + ": No such plugin " + arg)


#            elif cmd == "loaded":
#                if self.server.pluginManager.loadedPlugin(args[0]):
#                    self.server.doMessage(channel, user + ": Plugin " + args[0] + " is loaded")
#                else:
#                    if self.server.pluginManager.pluginExists(args[0]):
#                        self.server.doMessage(channel, user + ": Plugin " + args[0] + " is not loaded")
#                    else:
#                        self.server.doMessage(channel, user + ": Plugin " + args[0] + " does not exist")
        else:
            self.server.doMessage(channel, user + ": You are not authorized to use this function. ")
