# Copyright (C) 2010-2016 Svetlana A. Tkachenko <svetlana@members.fsf.org>
# Copyright (C) 2010-2016 David Vo <david.vo2@gmail.com>
# Copyright (C) 2010-2016 Ola Nystrom <olanys@gmail.com>
#
# This file is part of guppy.
#
# guppy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# guppy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with guppy.  If not, see <http://www.gnu.org/licenses/>.

import time


@plugin
class ctcp(object):
    """Add support for CTCP source, CTCP version."""
    def __init__(self, server):
        self.prnt = server.prnt
        self.server = server
        self.commands = []
        self.server.handle("message", self.handle_message)

    def handle_message(self, channel, nick, message):
        if message.find("\001") == -1:
            return
        message_array = message.replace("\001", "", 2).strip().lower().split()
        ctcp_type = message_array[0]
        if len(message_array) > 1:
            ctcp_args = " ".join(message_array[1:])
        else:
            ctcp_args = ""
        self.prnt("CTCP typ: " + ctcp_type + " came in")
        self.prnt("CTCP args: " + ctcp_args + " came in")
        if ctcp_type == "version":
            self.server.doNotice(nick, "\001VERSION guppy " + self.server.config["version"] + "\001")
        elif ctcp_type == "clientinfo":
            self.server.doNotice(nick, "\001VERSION guppy " + self.server.config["version"] + "\001")
        elif ctcp_type == "source":
            self.server.doNotice(nick, "\001VERSION http://repo.or.cz/w/guppy.git\001")
        elif ctcp_type == "ping":
            self.server.doNotice(nick, "\001PING " + ctcp_args + "\001")
        elif ctcp_type == "time":
            self.server.doNotice(nick, "\001TIME " + time.strftime("%a %b %d %H:%M:%S %Y %z") + "\001")
